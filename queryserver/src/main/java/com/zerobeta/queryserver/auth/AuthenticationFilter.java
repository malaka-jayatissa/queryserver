package com.zerobeta.queryserver.auth;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class AuthenticationFilter extends GenericFilterBean {
    
    @Value("${application.authentication.url}")
    private String authClient;

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);
    @Override
    public void doFilter(
      ServletRequest request, 
      ServletResponse response,
      FilterChain chain) throws IOException, ServletException {

        logger.info("doFilter");
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        Enumeration<String> headerNames = httpRequest.getHeaderNames();

        String authHeader = httpRequest.getHeader("Authorization");
        logger.info(authHeader);
        //Todo move to new class
        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(authClient).path("verify");
        
        builder.queryParam("auth_type", "BASIC");
        builder.queryParam("auth_details", authHeader);
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<?> entity = new HttpEntity<>(headers);
        logger.info(builder.toUriString()); //Todo remove
        ResponseEntity<String> authServierResponse
        = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, String.class);

        if(authServierResponse.getStatusCode() == HttpStatus.OK)
        {
            chain.doFilter(request, response);
        }
        else
        {
            logger.info("Authentication failed");
            return;
        }
    }


}
