package com.zerobeta.queryserver.repository;
import java.util.List;

import com.zerobeta.queryserver.model.SaleFeedDB;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;



public interface QueryServerRepository extends JpaRepository<SaleFeedDB, Integer>  {

    public List<SaleFeedDB> findByPosTransactionTimeBetween(String startTime, String endTime);
    
    public List<SaleFeedDB> findByPosTransactionTimeLike(String query);
    
}
