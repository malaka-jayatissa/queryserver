package com.zerobeta.queryserver.controller;

import java.util.List;

import com.zerobeta.queryserver.model.SaleFeedDB;
import com.zerobeta.queryserver.model.SalesSummary;
import com.zerobeta.queryserver.repository.QueryServerRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QueryServerController {

    private static final Logger logger = LoggerFactory.getLogger(QueryServerController.class);

    @Autowired
    QueryServerRepository repository;

    @RequestMapping(value = "salsefeed", method= RequestMethod.GET)
    public ResponseEntity<List<SaleFeedDB>> salsefeed(@RequestParam String startTime, @RequestParam String endTime){
        List<SaleFeedDB> feedList = repository.findByPosTransactionTimeBetween(startTime, endTime);
        for(SaleFeedDB feed: feedList)
        {
            logger.info(feed.toString());
        }
        return new ResponseEntity<>(feedList,HttpStatus.OK);
    }

    @RequestMapping(value = "monthlysales", method= RequestMethod.GET)
    public ResponseEntity<SalesSummary> montlySummery( @RequestParam String month){

        logger.info(month);
       // List<SaleFeedDB> feedList = repository.findMonthlySummeryData(businessID,month);
       String monthLike = month+ "%";
       List<SaleFeedDB> feedList = repository.findByPosTransactionTimeLike(monthLike);
       SalesSummary summary = new SalesSummary();
       summary.setMonth(month);
        for(SaleFeedDB feed: feedList)
        {
            logger.info(feed.toString());
            summary.setPosTransactionAmount(summary.getPosTransactionAmount() + feed.getPosTransactionAmount());
            summary.setPosDiscount(summary.getPosDiscount()+ feed.getPosDiscount());
        }
        return new ResponseEntity<>(summary, HttpStatus.OK);
    }

    @RequestMapping(value = "feedrate", method= RequestMethod.GET)
    public ResponseEntity<String> feedRate(){
        return new ResponseEntity<>("ok", HttpStatus.OK);
        //Todo Implement
    }
}
