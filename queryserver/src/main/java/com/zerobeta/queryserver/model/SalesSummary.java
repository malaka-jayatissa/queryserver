package com.zerobeta.queryserver.model;

public class SalesSummary {
    String month;
    float posTransactionAmount;
    float posDiscount;
    public String getMonth() {
        return month;
    }
    public void setMonth(String month) {
        this.month = month;
    }
    public float getPosTransactionAmount() {
        return posTransactionAmount;
    }
    public void setPosTransactionAmount(float posTransactionAmount) {
        this.posTransactionAmount = posTransactionAmount;
    }
    public float getPosDiscount() {
        return posDiscount;
    }
    public void setPosDiscount(float posDiscount) {
        this.posDiscount = posDiscount;
    }
    
    @Override
    public String toString() {
        return "SalesSummary [month=" + month + ", posDiscount=" + posDiscount + ", posTransactionAmount="
                + posTransactionAmount + "]";
    }
    
    
    
}
